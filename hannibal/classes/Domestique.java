package hannibal.classes;

import hannibal.interfaces.Animal;

public class Domestique implements Animal {
	int age = 0;

	public void die(String reason) {
		System.out.println("il meurt de " + reason);
		
	}

	public void eat() {
		// TODO Auto-generated method stub
		
	}

	public void breath() {
		// TODO Auto-generated method stub
		
	}
	
	public int getAge() {
		return age;
	}
	
	public void vieillir() {
		age++;
	}
	
	public void vieillir(int annees) {
		age += annees;
	}
}