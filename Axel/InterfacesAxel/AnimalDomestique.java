// Cet interface fait partie du package Axel.InterfacesAxel
package Axel.InterfacesAxel;

/* On implémente l'interface Animals dans AnimalDomestique
 * On ajoute des caractéristiques à l'interface AnimalDomestique, tout en reprenant les caractéristiques de l'interface
 */
public interface AnimalDomestique extends Animals {
	public void faitDesCalins ();
	public void mangeDesCroquettes ();
	public void faitLeBeau ();
	
	public static void main(String[] args) {
	}
}
