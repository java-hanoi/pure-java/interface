// Cet interface fait partie du package Axel.InterfacesAxel
package Axel.InterfacesAxel;

// Définit les caractéristiques de l'interface Animals : Manger et Dormir
public interface Animals {
	public void manger ();
	public void dormir ();

	public static void main(String[] args) {
	}
}
