// Cette classe fait partie du package Axel.ClassesAxel
package Axel.ClassesAxel;
// On importe le package Axel.InterfacesAxel pour le relier � cette classe
import Axel.InterfacesAxel.*;

/* On impl�mente l'interface AnimalDomestique � la classe Chat
 * On ajoute des caract�ristiques � la classe Chat, tout en reprenant les caract�ristiques de l'interface
 */
public class Chat implements AnimalDomestique {
	public void faitDesCalins () {
		System.out.println("Je suis tout mignon");
	}
	public void mangeDesCroquettes () {
	System.out.println("Je mange des croquettes et c'est vraiment d�gueulasse");
	}
	public void faitLeBeau () {
	System.out.println("Je fait le beau pour avoir des croquettes que j'aime pas");
	}
	public void manger () {
		System.out.println("Je fais des ronron quand je mange");
	}
	public void dormir () {
		System.out.println("Je dors 26h sur 24");
	}

// On cr�e un objet, de la classe Chat, qui aura les caract�ristiques de cette classe
	public static void main(String[] args) {
		Chat minouminou = new Chat (); // Nom bizarre pour un chat !
		minouminou.mangeDesCroquettes();
		
		Chat puteuuh = new Chat (); // Nom TRES bizarre pour un chat !
		puteuuh.manger ();
		
	
	}
}
