// Cette classe fait partie du package Axel.ClassesAxel
package Axel.ClassesAxel;
//On importe le package Axel.InterfacesAxel pour le relier � cette classe
import Axel.InterfacesAxel.*;

/* On impl�mente l'interface AnimalSauvage � la classe Loup
 * On ajoute des caract�ristiques � la classe Loup, tout en reprenant les caract�ristiques de l'interface
 */
public class Loup implements AnimalSauvage { 
	public void resteDehors () {
		System.out.println("Je reste dehors grace a mon super pelage !");
	}
	public void mangeLesHommes () {
	System.out.println("Je mange que les cuisses d'humain");
	}
	public void manger () {
		System.out.println("Je mange pas, je te d�vore");
	}
	public void dormir () {
		System.out.println("Je dors 5 minutes pour te bouffer");
	}
	public static void main(String[] args) {
	}
	
// Tu n'as pas cr�� d'objets dans cette classe ! CA NE SERT A RIEN !
}
