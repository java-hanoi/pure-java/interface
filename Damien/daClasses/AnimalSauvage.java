package Damien.daClasses;
import Damien.daInterfaces.*;

public class AnimalSauvage implements Animal {
	
	public void respiration () {
		System.out.println ("Poumons"); //On respire par le nez et grace � des poumons! Je ne vois pas ou se trouve le nez dans cette histoire
	}
	
	public void d�placement () {
		System.out.println ("Courir");
	}
	
	public void nbrePattes () {
		System.out.println ("4");
	}
	
	public void nbreYeux () { // On dit "NbreOeil" qui au pluriel devient Yeux
		System.out.println ("2");
	}
	
	public void petitanimal () { //Je ne comprend pas cette d�marche ! Petit pour �tre grand ?! ALERTE AU GOGOL /!\
		System.out.println ("Est un grand animal"); // Peut �tre remplacer par "Taille animal"
	}
	
	public void habitat () {
		System.out.println ("Habite dans la nature");
	}
	
	public static void main(String[] args) { //String ficelle
		AnimalSauvage tigre = new AnimalSauvage ();
		tigre.respiration();
		tigre.d�placement();
		tigre.nbrePattes();
		tigre.nbreYeux();
		tigre.petitanimal();
		tigre.habitat();
	}
}