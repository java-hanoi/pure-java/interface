package Damien.daClasses;
import Damien.daInterfaces.Animal; //Ne pas oublier de changer le nom lorsqu'on pull les donn�s 


public class AnimalDomestique implements Animal {
	
	public void respiration () {
		System.out.println ("Poumons");
	}
	
	public void d�placement () {
		System.out.println ("Marcher");
	}
	
	public void nbrePattes () {
		System.out.println ("4");
	}
	
	public void nbreYeux () {
		System.out.println ("2");
	}
	
	public void petitanimal () {
		System.out.println ("Est un petit animal");
	}
	
	public void habitat () {
		System.out.println ("Habite dans la maison");
	}
	
	public static void main(String[] args) {
		AnimalDomestique chien = new AnimalDomestique ();
		chien.respiration();
		chien.d�placement();
		chien.nbrePattes();
		chien.nbreYeux();
		chien.petitanimal();
		chien.habitat();
	} //Il n'y a pas que des chiens chez les annimaux domestiques, je t'invites � revoir tes classiques !
}
