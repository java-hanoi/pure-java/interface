package classes;
import interfaces.Motorise;
import interfaces.Manuel;

public class Main {

	public static void main(String[] args) {
		Motorise v = new Voiture();
		v.demarer();
        v.fairePlein();
        v.reculer();
        
        Manuel eltVelo = new Velo();
        eltVelo.pedaler();
        eltVelo.freiner();
        
	}

}
