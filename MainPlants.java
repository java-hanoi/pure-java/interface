import classesmep.*;

public class MainPlants {
	public static void main(String[] args) {
		Conifers myConifers = new Conifers();
		myConifers.photosynthesis("Tree ");
		myConifers.setseasons("winter.");
		myConifers.setuses("Christmas tree : ");
		
		System.out.print("");
		
		Leafy myLeafy = new Leafy();
		myLeafy.photosynthesis("Oak ");
		myLeafy.setseasons("spring.");
		myLeafy.setuses("oak barrel; ");
	}
}