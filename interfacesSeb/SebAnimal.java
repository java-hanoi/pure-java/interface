package interfacesSeb;

public interface SebAnimal {
	public void eat();
	public void breath();
	public void sleep();
}
