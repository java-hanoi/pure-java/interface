package irclasses;

import irInterfaces.Animal;

public class AnimalDomestique implements Animal{
	

	public void eat() {
		System.out.println("AnimalDomestique eat");
	}
	public void travel() {
		System.out.println("AnimalDomestique travel");
	}
	public static void main (String args[]) {
		AnimalDomestique medor = new AnimalDomestique ();
		medor.eat();
		medor.travel();
	}
}
