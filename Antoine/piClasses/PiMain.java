package Antoine.piClasses;

public class PiMain {
	public static void main (String args[]) {
		PiChien pluto = new PiChien();
		pluto.noise();
		pluto.grow(50);
		
		PiChien boul = new PiChien();
		boul.noise();
		System.out.println(String.format("valeur de myNoise: %s", boul.myNoise));
		boul.noise("yolo");
		System.out.println(String.format("valeur de myNoise: %s", boul.myNoise));
		boul.noise();
		boul.noise("ytigfy");
		System.out.println(String.format("valeur de myNoise: %s", boul.myNoise));
		
		boul.grow(3);
		
		PiChien bill = new PiChien(); 
		// Idefix is better! Bill is not a dog just so you know (-_-) 
		bill.noise("yo");
		bill.grow(3);
		
		PiChat felix = new PiChat();
		felix.noise();
	}
}
