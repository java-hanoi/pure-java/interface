package Antoine.piClasses;

public class PiChien extends PiAnimalDomestique {
	String myNoise = "whouf";
	public void noise () {
		System.out.println(String.format("I say %s", myNoise));
	}
	
	public void noise (String waf) {
		myNoise = waf;
		System.out.println(String.format("I say %s", waf));
		/* In my opinion if you put a different message 
		 * than "I say" every time it will be easier to understand 
		 * that you are testing something 
		 */
	}
}
