package Antoine.piClasses;
import Antoine.piInterfaces.PiAnimal;

public class PiAnimalDomestique implements PiAnimal {
	public void run () {
		System.out.println("Domestical animals run");
	}
	public void run (int duration) {
		System.out.println("Domestical animals run");
	}

	public void grow(int size) {
		System.out.println(String.format("My size is %d", size));
	}
}