import soClasses.SoCat;
import soClasses.SoDog;

public class SoMain {

	public static void main(String[] args) {
		SoDog myDog = new SoDog();
		myDog.setDeath("a cancer");
		myDog.death(8);
		myDog.eat();
		myDog.setEat("meat");
		
		System.out.println("-------------------------------");
		
		SoCat myCat = new SoCat();
		myCat.setDeath("a lungs infection");
		myCat.death(11);
		myCat.eat();
		myCat.setEat("goldfish");
	}

}
