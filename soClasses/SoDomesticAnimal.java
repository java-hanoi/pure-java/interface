package soClasses;

import soInterfaces.SoAnimal;

public class SoDomesticAnimal implements SoAnimal {
	public void setDeath(String reason) {
		System.out.println("Domestic animals are usually dying because of "+reason);
	}

	public void death(int age) {
		System.out.println("Domestic animals are statistically dying at the age of "+age);
	}
	
	public void eat() {
		System.out.println("Domestic animals can eat");
	}
	public void setEat(String food) {
		System.out.println("Domestic animals like to eat "+food);
	}

	public static void main(String[] args) {
		SoDomesticAnimal domesticAnimal = new SoDomesticAnimal();
		domesticAnimal.setDeath("old age");
		domesticAnimal.death(15);
		domesticAnimal.eat();
		domesticAnimal.setEat("vegetables and meat");
	}

}
