package soClasses;

import soInterfaces.SoAnimal;

public class SoCat implements SoAnimal{
	public void setDeath(String reason) {
		System.out.println("My cat died because of "+reason);
	}

	public void death(int age) {
		System.out.println("My cat died at the age of "+age);
	}
	
	public void eat() {
		System.out.println("My cat can eat");
	}
	public void setEat(String food) {
		System.out.println("My cat likes to eat "+food);
	}
	
	String meow;
	
	public SoCat() {
		System.out.println("Miaou Miaou!");
		meow = "Inconnu";
	}
		
}
