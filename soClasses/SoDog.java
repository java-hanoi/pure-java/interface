package soClasses;

import soInterfaces.SoAnimal;

public class SoDog implements SoAnimal{
	public void setDeath(String reason) {
		System.out.println("My dog died because of "+reason);
	}

	public void death(int age) {
		System.out.println("My dog died at the age of "+age);
	}
	
	public void eat() {
		System.out.println("My dog can eat");
	}
	public void setEat(String food) {
		System.out.println("My dog likes to eat "+food);
	}
	
	String bark;
	
	public SoDog() {
		System.out.println("Waf Waf!");
		bark = "Inconnu";
	}

}
