package rt_alive.rt_classes;

import rt_alive.rt_interfaces.Rt_Pets;

public class Rt_Dog implements Rt_Pets {
	public Rt_Dog(String name) {
		this.name = name;
		System.out.println("Nouveau chien adopt�!");
		System.out.println("Son nom est " + name + "il est vivant car il");
	}

	// LivingBeing
	public String breathing() {
		System.out.println("Respire");
		return "Respire";
	}

	public void eating() {
		System.out.println("Mange");
	}

	public void drinking() {
		System.out.println("Bois");
	}

	public void dying() {
		System.out.println("Meurs");
	}

	// Animal
	public void moving() {
		System.out.println("Bouge");
	}

	public void cry() {
		System.out.println("Aboie");
	}

	public void species() {
		System.out.println("Rottweiler");
	}

	// Pets
	public void wreckingSofas() {
		System.out.println("Je mange les fauteuils");
	}

	public void wakeYouUpAt3Am() {
		System.out.println("Je vous r�veille � 4 du mat' en vous posant ma laisse sur le visage");
	}

	String name = "Notre chien";
}
