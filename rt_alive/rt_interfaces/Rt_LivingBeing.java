package rt_alive.rt_interfaces;

public interface Rt_LivingBeing {
	public String breathing();
	public void eating();
	public void drinking();
	public void dying();
}
