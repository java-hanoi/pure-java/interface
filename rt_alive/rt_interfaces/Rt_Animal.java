package rt_alive.rt_interfaces;

public interface Rt_Animal extends Rt_LivingBeing{
	public void moving();
	public void cry();
	public void species();
}
