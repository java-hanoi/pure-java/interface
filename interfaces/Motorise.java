package interfaces;

public interface Motorise {
	
	public void fairePlein();
	
	public void demarer();
	
	public void reculer();
	
}
